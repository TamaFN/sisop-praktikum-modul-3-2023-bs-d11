# Kelompok D11 - SISOP D 
## 5025211196 - Sandyatama Fransisna Nugraha
## 5025211105 - Sarah Nurhasna Khairunnisa
## 5025211148 - Katarina Inezita Prambudi

<br>

---
# Soal 1

## Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!

<br>

### (Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

<br>

---
# Penyelesaian


## a. Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.

```R
void countFrequency(const char* filename, unsigned int freq[256]) {

    // Buka File
    FILE *file = fopen(filename, "r");
    
    // Jika file tidak bisa dibuka, maka akan keluar program
    if (file == NULL) {
        exit(1);
    }

    // Inisialisasi variabel i
    int i;
    
    // Membaca karakter dari file satu per satu
    while ((i = fgetc(file)) != EOF) {
        
        // Menambah jumlah frekuensi
        freq[i]++;
    }

    // Menutup file
    fclose(file);
}
```



## b. Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.

```R
// Fungsi untuk membuat huffman code
void buildCodeTable(Node* node, char* code, int length, char* huffCode[256]) {
    
    // Mengecek apakah node kosong
    if (node == NULL) {
        return;
    }

     // Mengecek apakah node punya child kanan atau kiri
    if (node->left == NULL && node->right == NULL) {
	
	// Mengeset karakter null
        code[length] = '\0';
        
	// Melakukan penyalinan dari code ke huffCode
        huffCode[node->data] = strdup(code);
    }

    // Mengeset karakter 0 pada indeks length
    code[length] = '0';
    
     // Melakukan rekursif
    buildCodeTable(node->left, code, length + 1, huffCode);

     // Mengeset karakter 1 pada indeks length
    code[length] = '1';
    
    // Melakukan rekursif
    buildCodeTable(node->right, code, length + 1, huffCode);
	
    // Mengecek length lebih dari 0	
    if (length > 0) {
        free(huffCode[node->data]);
    }
}
```


## c. Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.

```R
// Membuat fungsi writeHuffmanTree
void writeHuffmanTree(Node* node, FILE* file) {
    
    // Mengecek apakah node kosong
    if (node == NULL) {
        return;
    }

    // Mengecek apakah node punya child kanan atau kiri
    if (node->left == NULL && node->right == NULL) {
	
	// Menulis karakter 1 kedalam file
        fputc('1', file);
        
	// Menulis karakter yang disimpan dalam node ke dalam file
        fputc(node->data, file);
        
    //jika node memiliki child kiri dan kanan
    } else {
    
	// Menulis karakter 0 kedalam file 
        fputc('0', file);
        
        // Memanggil fungsi writeHUffmanTree untuk left
        writeHuffmanTree(node->left, file);
        
        // Memanggil fungsi writeHuffmanTree untuk right
        writeHuffmanTree(node->right, file);
    }
}
```

## d. Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 

```R
void compress(const char* inputFilename, const char* outputFilename, char* huffCode[256]) 
{  
    // Membuka input file
    FILE* inputFile = fopen(inputFilename, "r");
    
    // Mengecek file input apakah ada dan bisa dibuka
    if (inputFile == NULL) {
        exit(1);
    }
    
    // Membuka file output
    FILE* outputFile = fopen(outputFilename, "wb");
    
    // Mengecek file output apakah bisa dibuat 
    if (outputFile == NULL) {
        exit(1);
    }
    
    // Inisialisasi variable i
    int i;
    
    // Melakukan looping untuk membaca setiap karakter
    while ((i = fgetc(inputFile)) != EOF) {
    
	// Menyimpan kode huffman
        char* code = huffCode[i];
        
	// Menulis kode huffman
        fwrite(code, sizeof(char), strlen(code), outputFile);

    }
    
    // Menutup file inputFile
    fclose(inputFile);
    
    // Menutup file outputFile
    fclose(outputFile);
}
```


## e. Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.

```R
int main() 
{
    // Inisialisasi variable array freq 
    unsigned int freq[256] = {0};
    
    // Inisialisasi input_filename dengan menggunakan file.txt sebagai inputan
    const char* input_filename = "file.txt";
    
    // Inisialisasi compressed_filename dengan membuat file output baru yaitu compressed.txt
    const char* compressed_filename = "compressed.txt";
    
    // Memanggil fungsi countFrequency
    countFrequency(input_filename, freq);

    // Memanggil fungsi buildHuffmanTree
    Node* node = buildHuffmanTree(freq);

    // Inisialisasi pipe1
    int pipe1[2];
    
    // Mengecek pipe1 berhasil dibuat
    if (pipe(pipe1) == -1) {
        
        // Keluar program
        exit(1);
    }

    // Melakukan fork
    pid_t pid = fork();

    // Mengecek parent proses
    if (pid > 0) {
    
    	// Inisialisasi childFreq
        unsigned int childFreq[256];
        
        // Inisialisasi originalBits
        unsigned long long int originalBits = 0;
        
        // Inisialisasi compressedBits
        unsigned long long int compressedBits = 0;
        
        // Inisialisasi huffCode
        char* huffCode[256] = {NULL};
        
        
        // Inisialisasi code
        char code[256];
        
    	// Menutup file pipe1 untuk menulis
        close(pipe1[1]);
        
        // Membaca frekuensi karakter
        read(pipe1[0], childFreq, sizeof(childFreq));
        
    	// Inisialisasi i
    	int i;         
    	
    	// Melakukan looping sebanyak 256
        for (i = 0; i < 256; i++) {
        
            // Menghitung jumlah original bits
            originalBits += freq[i] * sizeof(char) * 8;
        }
        
	// Memanggil fungsi buildCodeTable
        buildCodeTable(node, code, 0, huffCode);

	// Membuka compressed_file
        FILE* compressed_file = fopen(compressed_filename, "wb");
        
        // Mengecek file compressed_file
        if (compressed_file == NULL) {
            exit(1);
        }
        
	// Memanggil fungsi writeHuffmanTree
        writeHuffmanTree(node, compressed_file);
        
	// Menutup compressed_file
        fclose(compressed_file);
        
	// Memanggil fungsi compress
        compress(input_filename, compressed_filename, huffCode);
        
	// Melakukan looping sebanyak 256
        for (i = 0; i < 256; i++) {
        
	    // Mengecek frekuensi karakter > 0 dan huffcode tidak NULL
            if (freq[i] > 0 && huffCode[i] != NULL) {
		
		// Menghitung compressedBits
                compressedBits += freq[i] * strlen(huffCode[i]);
            }
        }
        
    	// Mencetak Original Bits
        printf("Original Bits : %llu\n", originalBits);
        
        // Mencetak Compressed Bits
        printf("Compressed Bits: %llu\n", compressedBits);

        // Menunggu child Proses 
        wait(NULL);
        
    } else if (pid < 0) {
	exit(1);

    } else {
    	
    	// Menutup pipe1
        close(pipe1[0]);

        write(pipe1[1], freq, sizeof(freq));
        exit(0);
    }

    return 0;
}
```



### Catatan:
### Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda, contoh: 
- Huruf A	: 01000001
- Huruf a	: 01100001

### Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
### Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

<br>

---
## Dokumentasi Output

<br>

### Output program 1

![cronjobs](img/outputsoal1.png)

---
Tidak ada kendala
---

<br>

# Soal 2

## Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.
<br>

## a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

<br>

## b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
### (Catatan: wajib menerapkan konsep shared memory)

<br>

## c. Setelah ditampilkan, berikutnya untuk setiap angka dari     matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

<br> 

### Contoh: 
### array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

## maka:

### 1 2 6 24 120 720 ... ... …
### (Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

<br>

## d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 

<br>

## Dokumentasikan dan sampaikan saat demo dan laporan resmi.

---
## Penyelesaian
## 2a. Buat program kalian.c
1. Deklarasikan setiap matriks beserta baris dan kolomnya
    ```c
    #define ROW1 4 //baris matriks pertama = 4
    #define COL1 2 //kolom matriks pertama = 2
    #define ROW2 2 //baris matriks kedua = 2
    #define COL2 5 //kolom matriks kedua = 5
    ```
    ```c
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], result[ROW1][COL2];
    int i, j;
    ```
    <br>
2. Mengisi matriks 1 dengan angka random dari 1-5
    ```c
    srand(time(0));
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL1; j++){
            matriks1[i][j] = rand()%5 + 1;
        }
    }
    ```
    - Agar angka random yang diambil dapat berubah-ubah setiap kali program dijalankan, maka digunakan fungsi `srand(time(0));` sehingga program akan mengambil nilai waktu saat program dijalankan dan angka random yang dihasilkan akan berbeda. 
    - Kemudian agar rentang angka random dimulai dari 1-5, maka dilakukan operasi `rand()%5 + 1`.
    <br>
    <br>
3. Mengisi matriks 2 dengan angka random dari 1-4
    ```c
    for(i=0; i<ROW2; i++){
        for(j=0; j<COL2; j++){
            matriks2[i][j] = rand()%4 + 1;
        }
    }
    ```
    Agar rentang angka random dimulai dari 1-5, maka dilakukan operasi `rand()%4 + 1`.
    <br>
    <br>
4. Melakukan perkalian matriks 1 dan 2
    ```c
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL2; j++){
            result[i][j] = 0;
            for(int k=0; k<COL1; k++){
                result[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
    ```
    <br>
5. Menampilkan matriks pertama
    ```c
    printf("Matriks pertama:\n");
    printf("[");
    for (i=0; i<ROW1; i++) {
        printf("[");
        for (j=0; j<COL1; j++) {
            printf("%d", matriks1[i][j]);
            if (j < COL1-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");
    ```
    <br>
6. Menampilkan matriks kedua
    ```c
    printf("\nMatriks kedua:\n");
    printf("[");
    for (i=0; i<ROW2; i++) {
        printf("[");
        for (j=0; j<COL2; j++) {
            printf("%d", matriks2[i][j]);
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW2-1) {
            printf(",\n");
        }
    }
    printf("]\n");
    ```
    <br>
7. Menampilkan matriks hasil perkalian
    ```c
    printf("\nHasil perkalian matriks:\n");
    printf("[");
    for (i=0; i<ROW1; i++) {
        printf("[");
        for (j=0; j<COL2; j++) {
            printf("%d", result[i][j]);
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");
    ```
    <br>
8. Membuat shared memory
    ```c
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }
    
    int *shared_result = (int *)shmat(shmid, NULL, 0);
    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }
    ```
    <br>
9. Mengisi shared memory dengan matriks hasil perkalian
    ```c
    for (int i=0; i<ROW1; i++) {
        for (int j=0; j<COL2; j++) {
            *(shared_result + i*COL2 + j) = result[i][j];
        }
    }
    ```
    <br>
## 2b. Buat program cinta.c
1. Deklarasi
    ```c
    #define ROW1 4 //baris matriks hasil perkalian = 4
    #define COL2 5 //kolom matriks hasil perkalian = 5
    unsigned long long int faktorial;
    ```
    <br>
2. Buat shared memory dengan key yang sama pada program kalian.c pada fungsi main
    ```c
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);
    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }
    ```
    <br>
3. Menampilkan matriks hasil perkalian dari program kalian.c dengan menggunakan shared memory
    ```c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    printf("[");
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            printf("%d", *(shared_result + i*COL2 + j));
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");
    ```
    <br>

## 2c. Buat thread untuk perhitungan faktorial pada program cinta.c
1. Buat thread untuk melakukan perhitungan faktorial
    ```c
    void *hitung_faktorial(void *arg) {
        int num = *((int *)arg);
        faktorial = 1;
        for (int i=1; i<=num; i++) {
            faktorial *= i;
        }
        return (void *)faktorial;
    }
    ```
    <br>
2. Menghitung faktorial menggunakan thread `hitung_faktorial()`
    ```c
    pthread_t tid[ROW1*COL2];
    int index = 0;
    for (int i=0; i<ROW1; i++) {
        for (int j=0; j<COL2; j++) {
            int *arg = (int *)malloc(sizeof(*arg));
            *arg = *(shared_result + i*COL2 + j);
            pthread_create(&tid[index], NULL, hitung_faktorial, (void *)arg);
            index++;
        }
    }
    ```
    <br>
3. Menampilkan hasil perhitungan faktorial
    ```c
    printf("\nHasil faktorial:\n");
    printf("[");
    index = 0;
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            pthread_join(tid[index], (void *)&faktorial);
            printf("%llu", faktorial);
             if (j < COL2-1) {
                printf(", ");
            }
            index++;
        }
        printf("]");
        if (i < ROW1-1) {
            printf(", \n");
        }
    }
    printf("]\n");
    ```
    <br>
## 2d. Buat program sisop.c
1. Deklarasi
    ```c
    #define ROW1 4 //baris matriks hasil perkalian = 4
    #define COL2 5 //kolom matriks hasil perkalian = 5
    unsigned long long int faktorial;
    ```
    <br>
2. Buat fungsi untuk menghitung faktorial
    ```c
    unsigned long long int hitung_faktorial(int num) {
        faktorial = 1;
        for (int i=1; i<=num; i++) {
            faktorial *= i;
        }
        return faktorial;
    }
    ```
    <br>
3. Buat shared memory dengan key yang sama pada program kalian.c di fungsi main
    ```c
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);
    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }
    ```
    <br>
4. Menampilkan hasil perkalian matriks dari program kalian.c
    ```c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    printf("[");
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            printf("%d", *(shared_result + i * COL2 + j));
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");
    ```
    <br>
5. Menghitung dan menampilkan perhitungan faktorial menggunakan fungsi `hitung_faktorial()`
    ```c
    printf("\nHasil faktorial:\n");
    printf("[");
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            faktorial = hitung_faktorial(*(shared_result + i*COL2 + j));
            printf("%llu", faktorial);
            if(j < COL2-1){
                printf(", ");
            }
        }
        printf("]");
        if(i < ROW1-1){
            printf(", \n");
        }
    }
    printf("]\n");
    ```
---
---
## Dokumentasi Output
1. Output program kalian.c

![cronjobs](img/kalian.png)

2. Output program cinta.c

![cronjobs](img/cinta.png)

3. Output program sisop.c

![cronjobs](img/sisop.png)

4. Perbandingan program cinta.c dan sisop.c

![cronjobs](img/perbandingan_output1.png)

![cronjobs](img/perbandingan_output2.png)

#### Kesimpulannya, hasil yang diperoleh dari program sisop.c (tanpa thread) lebih cepat dibandingkan program cinta.c (dengan thread)
---
Tidak ada kendala
---

<br>

---
# Soal 3
## Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

## a. Bantulah Elshe untuk membuat sistem stream (receiver)  stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

## b. User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
## Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket sehingga struktur direktorinya adalah sebagai berikut:
```
└── soal3
	├── playlist.txt
	├── song-playlist.json
	├── stream.c
	└── user.c
```

## c. Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt
```
Sample Output:
17 - MK
1-800-273-8255 - Logic
1950 - King Princess
…
Your Love Is My Drug - Kesha
YOUTH - Troye Sivan
ZEZE (feat. Travis Scott & Offset) - Kodak Black
```

## d. User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.
```
PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":
PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"
```
Untuk mempermudah dan memperpendek kodingan, query bersifat tidak case sensitive 😀

## e. User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:
```
1. User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

2. User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”
```

## f. Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist. Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

## g. Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

<br>

### Catatan: 

### Untuk mengerjakan soal ini dapat menggunakan contoh implementasi message queue pada modul.
Perintah DECRYPT akan melakukan decrypt/decode/konversi dengan metode sebagai berikut:
```
ROT13
ROT13 atau rotate 13 merupakan metode enkripsi sederhana untuk melakukan enkripsi pada tiap karakter di string dengan menggesernya sebanyak 13 karakter.

Base64
Base64 adalah sistem encoding dari data biner menjadi teks yang menjamin tidak terjadinya modifikasi yang akan merubah datanya selama proses transportasi. Tools Decode/Encode.

Hex
Hexadecimal string merupakan kombinasi bilangan hexadesimal (0-9 dan A-F) yang merepresentasikan suatu string. Tools.
```
---
## Penyelesaian

## - stream.c
1. Deklarasikan user dan struct user yang berisi id dan command
    ```c
    int idUser1 = 1, idUser2 = 1, usrnum = 1;

    struct user {
	    int id;
	    char command[1024];
    };
    ```
    <br>
2. Buat fungsi untuk inisialisasi message queue
    ```c
    // Message queue
    int messageID() {
        key_t key;
        if((key = ftok("user", 65)) == -1) {
            perror("Error ftok\n");
            return 1;
        }

        int msgid;
        if((msgid = msgget(key, 0666 | IPC_CREAT)) == -1) {
            perror("Error msgid\n");
            return 1;
        }

    return msgid;
    }
    ```
    <br>
3. Buat fungsi untuk melakukan dekripsi menggunakan metode rot13
    ```c
    // Decrypt dengan metode rot13
    void rot13(FILE *file, char *str) {
        // Loop setiap karakter pada string
        for (int i=0; str[i]!='\0'; i++) {
            // Jika char berada dalam range A-M atau a-m,tambahkan ASCII dari char dengan 13
            if ((str[i] >= 'a' && str[i] <= 'm') || (str[i] >=      'A' && str[i] <= 'M')) {
               fputc((str[i] + 13), file);
            // Jika char berada dalam range N-Z atau n-z, kurangi ASCII dari char dengan 13
            } else if ((str[i] >= 'n' && str[i] <= 'z') || (str     [i] >= 'N' && str[i] <= 'Z')) {
                fputc((str[i] - 13), file);
            }
            // Jika char selain huruf, maka tidak ada perubahan dan langsung dimasukkan ke dalam file
            else {
                fputc(str[i], file);
            }
        }
    }
    ```
    <br>
4. Buat fungsi untuk melakukan dekripsi menggunakan metode base64
    ```c
    // Decrypt dengan metode base64
    char *base64(char *input, size_t input_lenght, size_t *output_lenght) {
        // Deklarasi basic input output
        BIO *bio, *b64;
        // Alokasi mememori untuk output
        char *output = (char*)malloc(input_lenght);
        // Inisialisasi memori output menjadi 0
        memset(output, 0, input_lenght); 
    
        // Create objek BIO
        bio = BIO_new_mem_buf(input, input_lenght); 
        b64 = BIO_new(BIO_f_base64()); 
        // Mengubah data pada bio menjadi data pada b64
        bio = BIO_push(b64, bio); 
    
        // Set flags untuk menghilangkan karakter baris baru yang biasanya ditambahkan oleh metode base64
        BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

        // Baca input dari BIO dan simpan dalam variabel output
        *output_lenght = BIO_read(bio, output, input_lenght); 
    
        BIO_free_all(bio); 
        return output;
    }
    ```
    <br>
5. Buat fungsi untuk melakukan dekripsi menggunakan metode hex
    ```c
    // Decrypt dengan metode hex
    void hex(FILE *file, char *str) {
        while (*str) {
            int value = 0;
            // Baca setiap 2 karakter
            for (int i=0; i<2 && *str; i++) {
                // Menggeser bit dari nilai value sebanyak 4 bit ke kiri.
                value <<= 4;
                // Cek apakah string merupakan bagian dari hexadesimal (0-9 dan A-F)
                if (*str >= '0' && *str <= '9') { 
                    value |= (*str - '0');
                } else if (*str >= 'a' && *str <= 'f') {
                    value |= (*str - 'a' + 10);
                } else if (*str >= 'A' && *str <= 'F') {
                    value |= (*str - 'A' + 10);
                } else {
                    fprintf(stderr, "Error: Invalid hexadecimal digit '%c'\n", *str);
                    return;
                }
                str++;
            }
            // Write value ke dalam file
            fputc(value, file);
        }
    }
    ```
6. Membuat fungsi dekripsi yang dapat melakukan 3 metode dengan memanfaatkan fungsi-fungsi yang telah dibuat sebelumnya.
    ```c
    // Fungsi dekripsi
    void decrypt() {
        // Open file song-playlist.json dengan mode read ("r")
        FILE *songPlaylist = fopen("song-playlist.json", "r");
        // Open file playlist.txt dengan mode write ("w")
        FILE *playlist = fopen("playlist.txt", "w");

        // Cek jika file gagal dibuka maka print error
        if(songPlaylist == NULL) {
            printf("Error: Failed to open file song-playlist.json\n");
            return;
        }
        if(playlist == NULL) {
            printf("Error: Failed to open file playlist.txt\n");
            return;
        }

        // Parsing JSON
        struct json_object *jsonObj = json_object_from_file("song-playlist.json");

        // Jika parsing gagal print error
        if(jsonObj == NULL) {
            printf("Error: Failed to parse JSON\n");
            fclose(songPlaylist);
            fclose(playlist);
            return;
        }

        struct json_object *jsonMethod, *jsonSong;
        char *method, *song;
        int lenght = json_object_array_length(jsonObj);

        // Loop sebanyak lenght
        for(int i=0; i<lenght; i++) {
            // Mendapatkan objek JSON
            struct json_object *jsonIndex = json_object_array_get_idx(jsonObj, i);

            // Mendapatkan value "method" dan "song" dari objek JSON
            if(json_object_object_get_ex(jsonIndex, "method", &jsonMethod)) {
                method = (char *)json_object_get_string(jsonMethod);
            }
            if(json_object_object_get_ex(jsonIndex, "song", &jsonSong)) {
                song = (char *)json_object_get_string(jsonSong);
            }

            // Jalankan dekripsi sesuai metode yang diberikan user
            if(strcasecmp(method, "rot13") == 0) {
                rot13(playlist, song);
            }
            if(strcasecmp(method, "hex") == 0) {
                hex(playlist, song);
            }
            if(strcasecmp(method, "base64") == 0) {
                size_t outputSize;
                char *outputData = base64(song, strlen(song), &outputSize);
                fwrite(outputData, 1, outputSize, playlist);
                free(outputData);
            }
            
            fwrite("\n", 1, 1, playlist);
        }

        fclose(songPlaylist);
        fclose(playlist);
    }
    ```
7. Buat fungsi untuk sorting playlist
    - Fungsi swap
        ```c
        // Fungsi swap
        void swap(char **a, char **b) {
            char *temp = *a;
            *a = *b;
            *b = temp;
        }
        ```
    - Fungsi partition untuk membagi array menjadi 2
        ```c
        // Fungsi partition
        int partition(char **lines, int left, int right) {
            char *pivot = lines[right];
            int i = left - 1;
            for (int j=left; j<right; j++) {
                if (strcasecmp(lines[j], pivot) < 0) {
                    i++;
                    swap(&lines[i], &lines[j]);
                }
            }
            swap(&lines[i + 1], &lines[right]);
            return i + 1;
        }
        ```
    - Fungsi quicksort untuk melakukan algoritma quicksort
        ```c
        // Fungsi quicksort
        void quicksort(char **lines, int left, int right) {
            if (left < right) {
                int pivot_idx = partition(lines, left, right);
                quicksort(lines, left, pivot_idx - 1);
                quicksort(lines, pivot_idx + 1, right);
            }
        }
        ```
    - Fungsi sorting sebagai fungsi utama yang akan memanfaatkan fungsi-fungsi sebelumnya (swap, partition, dan quicksort) dan akan melakukan sorting pada `playlist.txt`
        ```c
        void sort() {
            int MAX_LINE_LEN = 10000;
            FILE *fp = fopen("playlist.txt", "r");
            if (!fp) {
                printf("Error: cannot open file playlist.txt\n");
                return;
            }

            char **lines = malloc(sizeof(char*) * MAX_LINE_LEN);
            int num_lines = 0;
            char line[MAX_LINE_LEN];
            while (fgets(line, MAX_LINE_LEN, fp) != NULL) {
                lines[num_lines] = strdup(line);
                num_lines++;
            }
            fclose(fp);

            quicksort(lines, 0, num_lines - 1);

            fp = fopen("playlist.txt", "w");
            for (int i=0; i<num_lines; i++) {
                fprintf(fp, "%s", lines[i]);
                free(lines[i]);
            }
            fclose(fp);
            free(lines);

            printf("File successfully decrypted and sorted.\n");
        }
        ```
    <br>
8. Buat fungsi command `LIST` yang dapat digunakan oleh user untuk menampilkan semua hasil dekripsi dan sorting playlist
    ```c
    // Fungsi list untuk menampilkan semua list lagu yang ada pada file playlist.txt
    void list() {
        // Open file playlist dalam mode read ("r")
        FILE *filePointer = fopen("playlist.txt", "r");
        // Check if the file was opened successfully
        if (filePointer == NULL) {
            printf("File cannot be opened.\n");
            return;
        }

        char ch;
        // Baca karakter di dalam file sampai EOF
        while ((ch = fgetc(filePointer)) != EOF)
            printf("%c", ch);

        fclose(filePointer);
    }
    ```
    <br>
9. Buat fungsi command `PLAY` yang dapat digunakan oleh user untuk memutar lagu yang diinginkan dengan format `PLAY <SONG>`
    ```c
    // Fungsi play song
    void play(char* word, int id) {
        // Open file playlist dalam mode read ("r")
        FILE* file = fopen("playlist.txt", "r");
        if (!file) {
            printf("Failed to open playlist.txt\n");
            return;
        }

        char line[1000];
        // Array yang menampung lagu yang sesuai
        char list_song[1000][1000]; 
        // Variabel counter untuk menyimpan song yang sesuai
        int found_count = 0;

        char search_word[1000];
        int i;
        for (i=0; word[i]; i++) {
            search_word[i] = tolower(word[i]);
        }
        search_word[i] = '\0';

        // Baca file setiap line
        while (fgets(line, 1000, file)) {
            int line_length = strlen(line);

            char lower_line[1000];
            int j;
            for (j=0; j<line_length; j++) { 
                lower_line[j] = tolower(line[j]);
            }
            lower_line[j] = '\0';

            // Jika search term ada pada line di dalam file
            if (strstr(lower_line, search_word) != NULL) {
                // Print/tambahkan lagu ke dalam list
                sprintf(list_song[found_count], "%s", line); 
                // Counter akan bertambah
                found_count++; 
            }
        }

        // Jika hanya ada 1 lagu yang sesuai 
        if(found_count == 1) { 
            printf("USER <%d> PLAYING \"%s\"\n", id, list_song[0]);
        }
        // Jika tidak ada lagu yang sesuai
        else if (found_count == 0) { 
            printf("THERE IS NO SONG CONTAINING \"%s\"\n", word);
        }
        // Jika ada lebih dari 1 lagu yang sesuai
        else {
            printf("THERE ARE %d SONG CONTAINING \"%s\"\n", found_count, word);
            for (i=0; i<found_count; i++) {
                printf("%d. %s", i+1, list_song[i]);
            }
        }

        fclose(file);
    }
    ```
    
    Command  `PLAY` akan memberikan 3 output yang berbeda sesuai dengan list lagu yang ada.
    - Apabila hanya ada satu lagu yang sesuai dengan input user, maka program akan memberikan output:
        ```
        USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"
        ```
        Agar dapat menampilkan id user, maka digunakan id yang telah disimpan dalam variabel `id` pada fungsi `struct user()`.

    - Apabila ditemukan 2 atau lebih lagu yang sesuai dengan input yang diinputkan oleh user, maka program akan memberikan output:
        ```
        THERE ARE "N" SONG CONTAINING "BREAK":
        1. THE SCRIPT - BREAKEVEN
        2. ARIANA GRANDE - BREAK FREE`
        ```
    - Apabila tidak ada lagu yang sesuai dengan input yang diberikan oleh user, maka program akan menampilkan output:
        ```
        THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"
        ```
    <br>
10. Buat fungsi command `ADD` yang dapat digunakan oleh user untuk memutar lagu yang diinginkan dengan format `ADD <SONG1>`
    ```c
    // Fungsi add song
    void add(char* word, int id) {    
        // Deklarasi
        // Open file dalam mode baca ("r")
        FILE *fp = fopen("playlist.txt", "r");    
        char line[100];    

        // Baca sertiap line di dalam file playlist.txt
        while (fgets(line, 100, fp) != NULL) {   
            // cek apakah lagu yang ditambahkan sudah ada di dalam playlist
            if (strcasecmp(line, word) == 0) {    
                // jika iya, print: SONG ALREADY ON PLAYLIST
                printf("SONG ALREADY ON PLAYLIST\n");    

                fclose(fp);  
                return;    
            }
        }

        fclose(fp);   

        // Open file dalam mode append ("a") untuk menambahkan data ke akhir file
        fp = fopen("playlist.txt", "a");    
        // Add song
        fprintf(fp, "%s\n", word);    
        fclose(fp);    
        printf("USER %d ADD %s\n", id, word);    
    }
    ```

    Command  `ADD` akan memberikan 2 output yang berbeda sesuai dengan list lagu yang ada.
    - Apabila lagu yang diinputkan tidak ada di dalam playlist.txt, maka program akan memasukkan lagu ke dalam playlist dan akan mengeluarkan output:
        ```
        USER <ID_USER> ADD <SONG1>
        ```
    - Apabila lagu yang diinputkan sudah ada di dalam playlist.txt, maka program akan mengeluarkan output:
        ```
        SONG ALREADY ON PLAYLIST
        ```
    <br>
11. Buat fungsi main
    ```c
    int main() {
        // Buat message queue
        int msgid = messageID();
        if(msgid == -1) {
            printf("Error: Failed to create message queue\n");
            exit(EXIT_FAILURE);
        }

        // Inisialisasi semaphore
        sem_t sem;
        sem_init(&sem, 0, 2);

        // Buat variabel bernama user Data untuk menyimpan command dari user
        struct user userData;

        while(1) {
            // Menerima message dari message queue
            if(msgrcv(msgid, &userData, sizeof(userData.command), 0, 0) == -1){ 
                perror("Failed at receiving message\n");
                exit(1);
            }
            // Set the user number = 1, anggap user sebagai user pertama
            usrnum = 1;

            sem_wait(&sem);

            // Update user id jika user berubah
            if(idUser1 != 1 && idUser2 == 2 && idUser1 != userData.id){
                idUser2 = userData.id;
            }

            if(idUser1 == 1 && idUser2 != userData.id) {
                idUser1 = userData.id;
                if(idUser2 == 1) {
                    idUser2++;
                }
            }

            // Cek jika stream overload
            if(idUser1 != userData.id  && idUser2 != userData.id && idUser1 != 1 && idUser2 > 2) {
                printf("STREAM SYSTEM OVERLOAD\n");
                usrnum = 0;
            }

            // Jalankan command dari user
            if(usrnum == 1) {
                // Decrypt command
                if(strcasecmp(userData.command, "DECRYPT\n") == 0) {
                    decrypt();
                    sort();
                }
                // List command
                else if(strcasecmp(userData.command, "LIST\n") == 0) {
                    list();
                }
                // Play dan Add command
                else if(strstr(userData.command, "PLAY") != NULL || strstr(userData.command, "ADD") != NULL) {
                    char *start = strchr(userData.command, '"');
                    if (start != NULL) {
                        start++;
                        char *end = strchr(start, '"');
                        if (end != NULL) {
                            int len = end - start;
                            char output[len + 1];
                            strncpy(output, start, len);
                            output[len] = '\0';

                            // Play command
                            if(strstr(userData.command, "PLAY") != NULL) {
                                play(output, userData.id);
                            }
                            // Add command
                            if(strstr(userData.command, "ADD") != NULL) {
                                add(output, userData.id);
                            }
                        }
                    }
                }
                // Unknown command
                else {
                    printf("UNKNOWN COMMAND\n");
                }

                // Exit command
                if(strcasecmp(userData.command, "exit\n") == 0){
                    if(idUser1 == userData.id) {
                        idUser1 = 1;
                        usrnum = 0;
                    }
                else if(idUser2 == userData.id) {
                    idUser2 = 2;
                    usrnum = 0;
                }
            }
            }

            sem_post(&sem);
        }

        sem_destroy(&sem);

        return 0;
    }
    ```
<br>
<br>

## - user.c
1. Buat fungsi struct user dan messageID() yang sama seperti pada program stream.c
    - struct user
        ```c
        struct user {
        int id;
        char command[1024];
        };
        ```
    - messageID()
        ```c
        // Message queue
        int messageID() {
            key_t key;
            if((key = ftok("user", 65)) == -1) {
                perror("Error ftok\n");
                return 1;
            }

            int msgid;
            if((msgid = msgget(key, IPC_CREAT | 0666)) == -1) {
                perror("Error msgid\n");
                return 1;
            }

            return msgid;
        }
        ```
        <br>
2. Fungsi main
    ```c
    int main() {
        // Inisialisasi message queue
        int msgid = messageID();
        if(msgid < 0) {
            printf("message queue failed\n");
            exit(EXIT_FAILURE);
        }
        
        while(1) {
            struct user userData;
            // Dapatkan id user
            userData.id = getpid();

            // Input command
            printf("ENTER COMMAND : ");
            fgets(userData.command, 1024, stdin);

            // Break ketika command yang diberikan user adalah "exit"
            if(strcasecmp(userData.command, "exit\n") == 0)
                break;

            // Kirim message yang berisi command user ke message queue
            if(msgsnd(msgid, &userData, sizeof(userData.command), 0) == -1) {
                perror("Error msgsnd");
                exit(EXIT_FAILURE);
            };
        }

        return 0;
    }
    ```
    Pada program user hanya diperuntukkan untuk memasukkan input user berupa command. Kemudian command akan dieksekusi oleh program stream.c dan hasil akan ditampilkan oleh program stream.c sesuai dengan input command yang diberikan oleh user pada program user.c
---
---
## Dokumentasi Output
1. DECRYPT
<br>
![decrypt](img/decrypt.png)
<br>
2. LIST
<br>
![list](img/list.png)
<br>
3. PLAY
<br>
![play1](img/play1.png)
<br>
![play2](img/play2.png)
<br>
![play3](img/play3.png)
<br>
4. ADD
<br>
![add1](img/add1.png)
<br>
![add2](img/add2.png)
<br>
5. SYSTEM OVERLOAD
<br>
![overload](img/overload.png)
<br>
6. UNKNOWN COMMAND
<br>
![unknown](img/unknown.png)
<br>
7. EXIT
<br>
![exit](img/exit.png)
<br>
8. Playlist.txt
<br>
![playlist](img/playlist.png)
<br>

---
## Kendala

Belum sempat mengerjakan soal no 3 karena masih fokus mengerjakan nomor lainnya. Terdapat kendala saat melakukan message queue dan saat melakukan decrypt menggunakan metode base64.
<br>

---

# Soal 4

## Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

## a. Download dan unzip file tersebut dalam kode c bernama unzip.c.

<br>

## b. Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
## Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

<br>

## c. Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
### - extension_a : banyak_file
### - extension_b : banyak_file
### - extension_c : banyak_file
### - other : banyak_file

<br>

## d. Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

<br>

## e. Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

<br>

### - DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
### - DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
### - DD-MM-YYYY HH:MM:SS MADE [folder name]

<br>

## examples : 
###  - 02-05-2023 10:01:02 ACCESSED files
### - 02-05-2023 10:01:03 ACCESSED files/abcd
###  - 02-05-2023 10:01:04 MADE categorized
###  - 02-05-2023 10:01:05 MADE categorized/jpg
###  - 02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg

<br>

## Catatan:
### - Path dimulai dari folder files atau categorized
### - Simpan di dalam log.txt
### - ACCESSED merupakan folder files beserta dalamnya
### - Urutan log tidak harus sama
### - Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
### - Untuk menghitung banyaknya ACCESSED yang dilakukan.
### - Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
### - Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.


## Program ini terdiri dari empat fungsi utama dan fungsi main yang akan memanggil fungsi-fungsi tersebut. Fungsi utama tersebut adalah:

---

<br> 

# Unzip

<br>

## DownloadFile()
## Fungsi ini mendownload file dari link google drive yang sudah disediakan dengan fungsi system wget, mengunzip dengan system unzip dan menghapus folder zip 

<br>

```R
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>

void DownloadFile() {
  char *url = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";

  system("wget --no-check-certificate -O hehe.zip -q \"https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download\"");
  system("unzip -q hehe.zip");
  execlp("rm","-rf","hehe.zip",NULL);
}



int main() {
  DownloadFile(); 
}

```

<br>

# Categorize

<br>

# clear_log()
## Fungsi ini menggunakan fungsi access() untuk mengecek apakah file log.txt ada atau tidak. Jika file tersebut ada, maka fungsi remove() akan dipanggil untuk menghapus file tersebut. Fungsi access() digunakan untuk mengecek hak akses pada file. Fungsi ini mengembalikan nilai 0 jika file tersebut dapat diakses dan mengembalikan nilai -1 jika file tersebut tidak dapat diakses.

```R
void clear_log() {
    if (access("log.txt", F_OK) != 0) return;
    remove("log.txt");
}

```

<br>

# open_log()
## Fungsi ini menggunakan fungsi fopen() untuk membuka file log.txt dalam mode append ("a"). Fungsi ini juga menggunakan fungsi time() dan localtime() untuk mendapatkan waktu saat ini dalam format lokal. Fungsi fprintf() digunakan untuk menuliskan entri log yang berisi tanggal dan waktu saat ini ke dalam file log.txt.

```R
FILE* open_log() {
    FILE* log = fopen("log.txt", "a");
    time_t current_time = time(NULL);
    struct tm* local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    fprintf(log, "%02d-%02d-%04d %02d:%02d:%02d ",
        local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
        local_time->tm_hour, local_time->tm_min, local_time->tm_sec
    );

    return log;
}
```

<br>

# close_log()
## Fungsi ini menggunakan fungsi fclose() untuk menutup file log.txt.

```R
void close_log(FILE* log) {
    fclose(log);
}
```

<br>

# log_accessed()
## Fungsi ini memanggil fungsi open_log() untuk membuka file log.txt. Fungsi fprintf() digunakan untuk menuliskan entri log berisi informasi bahwa file atau direktori pada path telah diakses. Fungsi close_log() kemudian dipanggil untuk menutup file log.txt.

```R
void log_accessed(char* path) {
    FILE* log = open_log();
    fprintf(log, "ACCESSED %s\n", path);
    close_log(log);
}
```

<br>

# log_made()
## Fungsi ini memanggil fungsi open_log() untuk membuka file log.txt. Fungsi fprintf() digunakan untuk menuliskan entri log berisi informasi bahwa direktori pada path telah dibuat. Fungsi close_log() kemudian dipanggil untuk menutup file log.txt.

```R
void log_made(char* path) {
    FILE* log = open_log();
    fprintf(log, "MADE %s\n", path);
    close_log(log);
}
```
<br>

# log_moved()
## Fungsi ini memanggil fungsi open_log() untuk membuka file log.txt. Fungsi fprintf() digunakan untuk menuliskan entri log berisi informasi bahwa file dengan ekstensi ext pada src telah dipindahkan ke dst. Fungsi close_log() kemudian dipanggil untuk menutup file log.txt.

```R
void log_moved(char* ext, char* src, char* dst) {
    FILE* log = open_log();
    fprintf(log, "MOVED %s file : %s > %s\n", ext, src, dst);
    close_log(log);
}
```

<br>

# move()
## Fungsi ini menggunakan fungsi rename() untuk memindahkan file dari src ke dst. Fungsi rename() memindahkan file dengan cara mengubah nama file tersebut sehingga file tersebut berada pada direktori dst. Jika pemindahan gagal, fungsi printf() dipanggil untuk menampilkan pesan kesalahan. Jika pemindahan berhasil, fungsi log_moved() dipanggil untuk menuliskan entri log.

```R
void move(char* ext, char* src, char* dst) {
    if (rename(src, dst) != 0) {
        printf("Failed to move %s to %s\n", src, dst);
        exit(EXIT_FAILURE);
    }
    else log_moved(ext, src, dst);
}
```

<br>

# make()
## Fungsi ini menggunakan fungsi access() untuk mengecek apakah direktori pada path sudah ada atau belum. Jika direktori tersebut sudah ada, fungsi ini langsung mengembalikan. Jika tidak, fungsi ini menggunakan fungsi mkdir() untuk membuat direktori tersebut. Fungsi mkdir() menggunakan parameter 0777 untuk memberikan hak akses penuh pada direktori yang dibuat. Jika pembuatan direktori gagal, fungsi printf() dipanggil untuk menampilkan pesan kesalahan. Jika pembuatan berhasil, fungsi log_made() dipanggil untuk menuliskan entri log.

```R
void make(char* path) {
    if (access(path, F_OK) == 0) return;
    if (mkdir(path, 0777) != 0) {
        printf("Failed to make directory %s\n", path);
        exit(EXIT_FAILURE);
    }
    else log_made(path);
}
```

<br>

# lower_case()
## Fungsi ini menggunakan loop for untuk mengubah tiap karakter pada string str menjadi lowercase menggunakan fungsi tolower().

```R
char* lower_case(char* str) {
    for (int i = 0; str[i]; i++) str[i] = tolower(str[i]);
    return str;
}
```

<br>

# get_ext()
## Fungsi ini menggunakan fungsi strrchr() untuk mencari karakter '.' terakhir pada string filename. Jika karakter tersebut ditemukan, maka ekstensi file dapat diperoleh dengan mengambil substring yang dimulai dari karakter '.' tersebut. Jika tidak ditemukan, maka fungsi ini mengembalikan string kosong.

```R
char* get_ext(char* filename) {
    char* ext = strrchr(filename, '.');
    if (ext == NULL) return "";
    return ext + 1;
}
```

<br>

# categorize_file()
## Fungsi ini menggunakan fungsi lower_case() dan get_ext() untuk mendapatkan ekstensi file filename. Jika file tersebut tidak memiliki ekstensi, maka file tersebut tidak dikategorikan dan fungsi ini langsung mengembalikan. Jika file tersebut memiliki ekstensi, maka fungsi ini menggunakan fungsi make() untuk membuat direktori pada dst berdasarkan ekstensi file tersebut, jika direktori tersebut belum ada. Fungsi move() kemudian dipanggil untuk memindahkan file dari src ke direktori yang sesuai di dst.

```R
void categorize(char* src, char* ext, int* cnt, int max) {
    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    if (src_dir == NULL) {
        printf("Failed to open directory %s\n", src);
        exit(EXIT_FAILURE);
    }
    else log_accessed(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char new_src[BUFFER_SIZE];
            snprintf(new_src, BUFFER_SIZE, "%s/%s", src, src_ent->d_name);
            categorize(new_src, ext, cnt, max);
        }

        else if (src_ent->d_type == DT_REG) {
            char src_ext[10];
            strcpy(src_ext, get_ext(src_ent->d_name));
            if (strcmp(ext, "other") != 0 && strcmp(lower_case(src_ext), ext) != 0) continue;
            
            int dst_cnt = *cnt / 10 + 1;
            char dst[BUFFER_SIZE];
            
            if (dst_cnt > 1) snprintf(dst, BUFFER_SIZE, "categorized/%s (%d)", ext, dst_cnt);
            else snprintf(dst, BUFFER_SIZE, "categorized/%s", ext);
            make(dst);

            char filename[BUFFER_SIZE];
            snprintf(filename, BUFFER_SIZE, "%s/%s", src, src_ent->d_name);
            strcat(dst, "/");
            strcat(dst, src_ent->d_name);
            move(ext, filename, dst);
            (*cnt)++;
        }
    }

    closedir(src_dir);
}
```

<br>

# sort_exts()
## Fungsi ini digunakan untuk melakukan sorting pada array exts berdasarkan jumlah file yang terkategorikan. Fungsi ini mengurutkan array exts dari yang terkecil ke terbesar.

```R
typedef struct {
    char name[BUFFER_SIZE];
    int cnt;
} Extensions;


void sort_exts(Extensions* exts, int size) {
    Extensions temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (exts[i].cnt < exts[j].cnt) continue;
            temp = exts[i];
            exts[i] = exts[j];
            exts[j] = temp;
        }
    }
}
```

<br>

# thread_function()
## Fungsi ini merupakan fungsi yang dijalankan oleh setiap thread yang dibuat pada program ini. Fungsi ini memanggil fungsi categorize() untuk mengkategorikan file-file dalam direktori. Fungsi ini juga mengembalikan struct Extensions yang berisi jumlah file yang terkategorikan berdasarkan ekstensi.

```R
typedef struct {
    char src[BUFFER_SIZE];
    char ext[10];
    int max;
} ThreadArgs;

void* thread_function(void* arg) {
    ThreadArgs* args = (ThreadArgs*) arg;
    int cnt = 0;
    categorize(args->src, args->ext, &cnt, args->max);

    Extensions* result = (Extensions*) malloc(sizeof(Extensions));
    strcpy(result->name, args->ext);
    result->cnt = cnt;

    return result;
}
```
<br>

# main()
## Fungsi utama dari program ini. Fungsi ini membaca file extensions.txt dan max.txt untuk mendapatkan daftar ekstensi dan batasan jumlah file yang terkategorikan untuk setiap ekstensi. Fungsi ini juga membuat sebuah thread untuk setiap ekstensi yang ada dan melakukan sorting pada array exts untuk menampilkan daftar ekstensi yang terkategorikan beserta jumlah file yang terkategorikan.

```R
int main() {
    clear_log();
    make("categorized");

    FILE* ext_fp = fopen("extensions.txt", "r");
    FILE* max_fp = fopen("max.txt", "r");

    int max;
    fscanf(max_fp, "%d", &max);

    char ext[10];
    int exts_size = 0;
    pthread_t tid[BUFFER_SIZE];
    Extensions exts[BUFFER_SIZE];
    ThreadArgs args[BUFFER_SIZE];

    while (fscanf(ext_fp, "%s", ext) != EOF) {
        strcpy(args[exts_size].src, "files");
        strcpy(args[exts_size].ext, ext);
        args[exts_size].max = max;
        pthread_create(&tid[exts_size], NULL, thread_function, &args[exts_size]);
        exts_size++;
    }

    for (int i = 0; i < exts_size; i++) {
        Extensions* result;
        pthread_join(tid[i], (void **) &result);
        exts[i] = *result;
    }

    int cnt = 0;
    categorize("files", "other", &cnt, max);
    strcpy(exts[exts_size].name, "other");
    exts[exts_size].cnt = cnt;
    exts_size++;

    sort_exts(exts, exts_size);
    for (int i = 0; i < exts_size; i++) {
        printf("%s : %d\n", exts[i].name, exts[i].cnt);
    }

    return 0;
}
```

<br>


# Logchecker

<br>

# sort_pairs()
## Fungsi ini digunakan untuk mengurutkan array folders yang berisi nama folder beserta jumlah file yang dipindahkan ke dalam folder tersebut secara menaik berdasarkan jumlah file yang terdapat di setiap folder. Fungsi ini menerima dua parameter, yaitu folders yang merupakan array of Pair berisi informasi nama folder dan jumlah file, dan size yang merupakan ukuran array tersebut. Fungsi ini menggunakan pendekatan bubble sort untuk melakukan pengurutan, yaitu dengan membandingkan nilai cnt pada pasangan elemen yang bersebelahan dan menukar posisinya jika diperlukan. Fungsi ini tidak mengembalikan nilai apapun karena array folders akan diurutkan secara langsung.

```R
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFER_SIZE 1024

typedef struct {
    char name[BUFFER_SIZE];
    int cnt;
} Pair;

void sort_pairs(Pair* folders, int size) {
    Pair temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (folders[i].cnt < folders[j].cnt) continue;
            temp = folders[i];
            folders[i] = folders[j];
            folders[j] = temp;
        }
    }
}
```

<br>

# main()
## Fungsi utama program ini. Fungsi ini pertama-tama memanggil command shell menggunakan fungsi system() untuk menampilkan jumlah akses pada file log.txt yang memiliki string "ACCESSED". Selanjutnya, program membuka file log.txt dan extensions.txt menggunakan fungsi popen() dan fopen().

```R
int main() {
    system("echo Total ACCESSED: $(cat log.txt | grep ACCESSED | wc -l)");

    FILE* folders_fp = popen("awk -F 'MADE ' '/MADE/ {print $2}' log.txt", "r");
    FILE* exts_fp = fopen("extensions.txt", "r");
    char buf[BUFFER_SIZE];

    printf("\nFolder Created : Total Files\n\n");

    int folders_size = 0;
    Pair folders[BUFFER_SIZE];
    while(fscanf(folders_fp, "%[^\n]%*c", buf) != EOF) {
        if (strcmp(buf, "") == 0) break;
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep '%s' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(folders[folders_size].name, buf);
        folders[folders_size].cnt = cnt;
        folders_size++;
    }

    sort_pairs(folders, folders_size);
    for (int i = 0; i < folders_size; i++) {
        printf("%s : %d\n", folders[i].name, folders[i].cnt);
    }

    printf("\nExtension Types : Total Files\n\n");

    int exts_size = 0;
    Pair exts[BUFFER_SIZE];
    while (fscanf(exts_fp, "%s", buf) != EOF) {
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep -i '\\.%s$' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(exts[exts_size].name, buf);
        exts[exts_size].cnt = cnt;
        exts_size++;
    }

    sort_pairs(exts, exts_size);
    for (int i = 0; i < exts_size; i++) {
        printf("%s : %d\n", exts[i].name, exts[i].cnt);
    }

    pclose(folders_fp);

    return 0;
}
```

<br>

## Setelah itu, program mencetak judul "Folder Created : Total Files" dan membaca nama folder yang telah dibuat beserta jumlah file yang dipindahkan ke dalam folder tersebut dari file log.txt menggunakan command shell yang dijalankan oleh fungsi popen(). Setiap nama folder dan jumlah file yang dipindahkan ke dalam folder tersebut disimpan pada array of Pair folders. 

<br>

## Selanjutnya, fungsi sort_pairs() dipanggil untuk mengurutkan array folders secara menaik berdasarkan jumlah file yang terdapat di setiap folder. Setiap nama folder beserta jumlah file yang dipindahkan ke dalam folder tersebut kemudian dicetak menggunakan perintah printf(). Setelah itu, program mencetak judul "Extension Types : Total Files" dan membaca jenis ekstensi file beserta jumlah file yang menggunakan ekstensi tersebut dari file extensions.txt. Setiap jenis ekstensi file dan jumlah file yang menggunakan ekstensi tersebut disimpan pada array of Pair exts. 

<br>

## Setelah itu, fungsi sort_pairs() dipanggil untuk mengurutkan array exts secara menaik berdasarkan jumlah file yang menggunakan ekstensi tersebut. Setiap jenis ekstensi file beserta jumlah file yang menggunakan ekstensi tersebut kemudian dicetak menggunakan perintah printf(). Terakhir, program menutup file log.txt dan extensions.txt menggunakan fungsi pclose() dan fclose(), dan program selesai.

<br>

## Dokumentasi Output
## 1. Output program unzip.c

![cronjobs](img/Unzip.png)
## 2. Output program modul 3

![cronjobs](img/HasilModul3.png)
## 3. Output program jumlah extension 

![cronjobs](img/JumlahExtension.png)
## 4. Output program dalam folder categorize

![cronjobs](img/DalamFolderCategorize.png)
## 5. Output program dalam folder files

![cronjobs](img/DalamFolderFiles.png)
## 6. Output program yang menghasilkan folder maximal 10

![cronjobs](img/Max10.png)

<br>

---

# Kendala

Masih bingung dalam penggunaan dan pengimplementasian thread, seperti pemindahan file menggunakan thread dan pembuatan log
---


