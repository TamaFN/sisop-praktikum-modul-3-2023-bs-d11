#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>


// digunakan untuk huffman tree
typedef struct Node {

    // Menyimpan karakter
    char data;
    
    // Menyimpan jumlah kemunculan
    unsigned freq;
    
    // Menunjukkan child
    struct Node *left, *right;
} Node ;


// Fungsi untuk membuat node
Node* createNode(char data, unsigned freq) {
    
    // Membuat sebuah node
    Node* node = (struct Node*)malloc(sizeof(struct Node));
    
    // Inisialiasi untuk node left
    node->left = NULL;
    
    // Inisialisasi untuk node right
    node->right = NULL;
    
    // Inisialisasi node data adalah data
    node->data = data;
    
    // Inisialisasi node freq adalah freq
    node->freq = freq;
    
    return node;
}

// Fungsi untuk menggabungkan node
Node* joinNode(Node* left, Node* right) {
    
    //Membuat node baru 
    Node* node = createNode('\0', left->freq + right->freq);
    
    //Child kiri
    node->left = left;
    
    //Child kanan
    node->right = right;
    
    return node;
}

// Membuat huffman tree
Node* buildHuffmanTree(unsigned int freq[256]) {
    
    //Membuat node baru
    Node* nodes[256] = {NULL};
    
    // Inisialisasi n_nodes    
    int n_nodes;
    
    // Inisialisasi variable i
    int i;
    
    // Melakukan looping sebanyak 256
    for (i = 0, n_nodes = 0; i < 256; i++) {
	
	// Melakukan pengecekan apakah freq > 0
        if (freq[i] > 0) {
	    
	    // Membuat node baru dengan memanggil createNode
            nodes[n_nodes] = createNode(i, freq[i]);
	    
	    // Melakukan penambahan 1 pada variable n_nodes
            n_nodes++;
        }
    }
    
    // Inisialisasi j
    int j = n_nodes;
    
    // Melakukan looping selama j > 1
    while (j > 1) {
	
	//Membuat node left
        Node *left = nodes[j - 2];
        
	//Membuat node right
        Node *right = nodes[j - 1];
	
	//Membuat node join dengan memanggil fungsi joinNode
        Node *join = joinNode(left, right);
	
	//mengganti node left dengan node join
        nodes[j - 2] = join;
	
	//Melakukan pengurangan 1 kepada variable j
        j--;
    }

    return nodes[0];
}

// Fungsi untuk menghitung frekuensi tiap karakter
void countFrequency(const char* filename, unsigned int freq[256]) {

    // Buka File
    FILE *file = fopen(filename, "r");
    
    // Jika file tidak bisa dibuka, maka akan keluar program
    if (file == NULL) {
        exit(1);
    }

    // Inisialisasi variabel i
    int i;
    
    // Membaca karakter dari file satu per satu
    while ((i = fgetc(file)) != EOF) {
        
        // Menambah jumlah frekuensi
        freq[i]++;
    }

    // Menutup file
    fclose(file);
}

// Membuat fungsi writeHuffmanTree
void writeHuffmanTree(Node* node, FILE* file) {
    
    // Mengecek apakah node kosong
    if (node == NULL) {
        return;
    }

    // Mengecek apakah node punya child kanan atau kiri
    if (node->left == NULL && node->right == NULL) {
	
	// Menulis karakter 1 kedalam file
        fputc('1', file);
        
	// Menulis karakter yang disimpan dalam node ke dalam file
        fputc(node->data, file);
        
    //jika node memiliki child kiri dan kanan
    } else {
    
	// Menulis karakter 0 kedalam file 
        fputc('0', file);
        
        // Memanggil fungsi writeHUffmanTree untuk left
        writeHuffmanTree(node->left, file);
        
        // Memanggil fungsi writeHuffmanTree untuk right
        writeHuffmanTree(node->right, file);
    }
}

// Fungsi untuk membuat huffman code
void buildCodeTable(Node* node, char* code, int length, char* huffCode[256]) {
    
    // Mengecek apakah node kosong
    if (node == NULL) {
        return;
    }

     // Mengecek apakah node punya child kanan atau kiri
    if (node->left == NULL && node->right == NULL) {
	
	// Mengeset karakter null
        code[length] = '\0';
        
	// Melakukan penyalinan dari code ke huffCode
        huffCode[node->data] = strdup(code);
    }

    // Mengeset karakter 0 pada indeks length
    code[length] = '0';
    
     // Melakukan rekursif
    buildCodeTable(node->left, code, length + 1, huffCode);

     // Mengeset karakter 1 pada indeks length
    code[length] = '1';
    
    // Melakukan rekursif
    buildCodeTable(node->right, code, length + 1, huffCode);
	
    // Mengecek length lebih dari 0	
    if (length > 0) {
        free(huffCode[node->data]);
    }
}

// Melakukan compress
void compress(const char* inputFilename, const char* outputFilename, char* huffCode[256]) 
{  
    // Membuka input file
    FILE* inputFile = fopen(inputFilename, "r");
    
    // Mengecek file input apakah ada dan bisa dibuka
    if (inputFile == NULL) {
        exit(1);
    }
    
    // Membuka file output
    FILE* outputFile = fopen(outputFilename, "wb");
    
    // Mengecek file output apakah bisa dibuat 
    if (outputFile == NULL) {
        exit(1);
    }
    
    // Inisialisasi variable i
    int i;
    
    // Melakukan looping untuk membaca setiap karakter
    while ((i = fgetc(inputFile)) != EOF) {
    
	// Menyimpan kode huffman
        char* code = huffCode[i];
        
	// Menulis kode huffman
        fwrite(code, sizeof(char), strlen(code), outputFile);

    }
    
    // Menutup file inputFile
    fclose(inputFile);
    
    // Menutup file outputFile
    fclose(outputFile);
}


int main() 
{
    // Inisialisasi variable array freq 
    unsigned int freq[256] = {0};
    
    // Inisialisasi input_filename dengan menggunakan file.txt sebagai inputan
    const char* input_filename = "file.txt";
    
    // Inisialisasi compressed_filename dengan membuat file output baru yaitu compressed.txt
    const char* compressed_filename = "compressed.txt";
    
    // Memanggil fungsi countFrequency
    countFrequency(input_filename, freq);

    // Memanggil fungsi buildHuffmanTree
    Node* node = buildHuffmanTree(freq);

    // Inisialisasi pipe1
    int pipe1[2];
    
    // Mengecek pipe1 berhasil dibuat
    if (pipe(pipe1) == -1) {
        
        // Keluar program
        exit(1);
    }

    // Melakukan fork
    pid_t pid = fork();

    // Mengecek parent proses
    if (pid > 0) {
    
    	// Inisialisasi childFreq
        unsigned int childFreq[256];
        
        // Inisialisasi originalBits
        unsigned long long int originalBits = 0;
        
        // Inisialisasi compressedBits
        unsigned long long int compressedBits = 0;
        
        // Inisialisasi huffCode
        char* huffCode[256] = {NULL};
        
        
        // Inisialisasi code
        char code[256];
        
    	// Menutup file pipe1 untuk menulis
        close(pipe1[1]);
        
        // Membaca frekuensi karakter
        read(pipe1[0], childFreq, sizeof(childFreq));
        
    	// Inisialisasi i
    	int i;         
    	
    	// Melakukan looping sebanyak 256
        for (i = 0; i < 256; i++) {
        
            // Menghitung jumlah original bits
            originalBits += freq[i] * sizeof(char) * 8;
        }
        
	// Memanggil fungsi buildCodeTable
        buildCodeTable(node, code, 0, huffCode);

	// Membuka compressed_file
        FILE* compressed_file = fopen(compressed_filename, "wb");
        
        // Mengecek file compressed_file
        if (compressed_file == NULL) {
            exit(1);
        }
        
	// Memanggil fungsi writeHuffmanTree
        writeHuffmanTree(node, compressed_file);
        
	// Menutup compressed_file
        fclose(compressed_file);
        
	// Memanggil fungsi compress
        compress(input_filename, compressed_filename, huffCode);
        
	// Melakukan looping sebanyak 256
        for (i = 0; i < 256; i++) {
        
	    // Mengecek frekuensi karakter > 0 dan huffcode tidak NULL
            if (freq[i] > 0 && huffCode[i] != NULL) {
		
		// Menghitung compressedBits
                compressedBits += freq[i] * strlen(huffCode[i]);
            }
        }
        
    	// Mencetak Original Bits
        printf("Original Bits : %llu\n", originalBits);
        
        // Mencetak Compressed Bits
        printf("Compressed Bits: %llu\n", compressedBits);

        // Menunggu child Proses 
        wait(NULL);
        
    } else if (pid < 0) {
	exit(1);

    } else {
    	
    	// Menutup pipe1
        close(pipe1[0]);

        write(pipe1[1], freq, sizeof(freq));
        exit(0);
    }

    return 0;
}

