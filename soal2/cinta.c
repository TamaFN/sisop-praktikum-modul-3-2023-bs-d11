#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROW1 4 //baris matriks hasil perkalian = 4
#define COL2 5 //kolom matriks hasil perkalian = 5
unsigned long long int faktorial;

// Thread untuk menghitung faktorial
void *hitung_faktorial(void *arg) {
    int num = *((int *)arg);
    faktorial = 1;
    for (int i=1; i<=num; i++) {
        faktorial *= i;
    }
    return (void *)faktorial;
}

int main() {
    // Membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    int *shared_result = (int *)shmat(shmid, NULL, 0);
    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Menampilkan matriks hasil perkalian dari program kalian.c
    printf("Hasil perkalian matriks dari program kalian.c:\n");
    printf("[");
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            printf("%d", *(shared_result + i*COL2 + j));
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");

    // Menghitung faktorial menggunakan thread hitung_faktorial()
    pthread_t tid[ROW1*COL2];
    int index = 0;
    for (int i=0; i<ROW1; i++) {
        for (int j=0; j<COL2; j++) {
            int *arg = (int *)malloc(sizeof(*arg));
            *arg = *(shared_result + i*COL2 + j);
            pthread_create(&tid[index], NULL, hitung_faktorial, (void *)arg);
            index++;
        }
    }

    // Menampilkan hasil faktorial
    printf("\nHasil faktorial:\n");
    printf("[");
    index = 0;
    for (int i=0; i<ROW1; i++) {
        printf("[");
        for (int j=0; j<COL2; j++) {
            pthread_join(tid[index], (void *)&faktorial);
            printf("%llu", faktorial);
             if (j < COL2-1) {
                printf(", ");
            }
            index++;
        }
        printf("]");
        if (i < ROW1-1) {
            printf(", \n");
        }
    }
    printf("]\n");

    return 0;
}