#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW1 4 //baris matriks pertama = 4
#define COL1 2 //kolom matriks pertama = 2
#define ROW2 2 //baris matriks kedua = 2
#define COL2 5 //kolom matriks kedua = 5

int main(){
    // Deklarasi
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], result[ROW1][COL2];
    int i, j;

    // Mengisi matriks 1 dengan angka random dari 1-5
    srand(time(0));
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL1; j++){
            matriks1[i][j] = rand()%5 + 1;
        }
    }

    // Mengisi matriks 2 dengan angka random dari 1-4
    for(i=0; i<ROW2; i++){
        for(j=0; j<COL2; j++){
            matriks2[i][j] = rand()%4 + 1;
        }
    }

    // Perkalian matriks
    for(i=0; i<ROW1; i++){
        for(j=0; j<COL2; j++){
            result[i][j] = 0;
            for(int k=0; k<COL1; k++){
                result[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    // Menampilkan matriks pertama
    printf("Matriks pertama:\n");
    printf("[");
    for (i=0; i<ROW1; i++) {
        printf("[");
        for (j=0; j<COL1; j++) {
            printf("%d", matriks1[i][j]);
            if (j < COL1-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");

    // Menampilkan matriks kedua
    printf("\nMatriks kedua:\n");
    printf("[");
    for (i=0; i<ROW2; i++) {
        printf("[");
        for (j=0; j<COL2; j++) {
            printf("%d", matriks2[i][j]);
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW2-1) {
            printf(",\n");
        }
    }
    printf("]\n");

    // Menampilkan matriks hasil perkalian
    printf("\nHasil perkalian matriks:\n");
    printf("[");
    for (i=0; i<ROW1; i++) {
        printf("[");
        for (j=0; j<COL2; j++) {
            printf("%d", result[i][j]);
            if (j < COL2-1) {
                printf(", ");
            }
        }
        printf("]");
        if (i < ROW1-1) {
            printf(",\n");
        }
    }
    printf("]\n");

    // Membuat shared memory
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }
    
    int *shared_result = (int *)shmat(shmid, NULL, 0);
    if (shared_result == (int *)-1) {
        perror("shmat");
        exit(1);
    }

    // Mengisi shared memory dengan matriks hasil perkalian matriks
    for (int i=0; i<ROW1; i++) {
        for (int j=0; j<COL2; j++) {
            *(shared_result + i*COL2 + j) = result[i][j];
        }
    }

    return 0;
}
