#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <json-c/json.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>
#include <semaphore.h>

int idUser1 = 1, idUser2 = 1, usrnum = 1;

struct user {
	int id;
	char command[1024];
};

// Message queue
int messageID() {
    key_t key;
    if((key = ftok("user", 65)) == -1) {
        perror("Error ftok\n");
        return 1;
    }

    int msgid;
    if((msgid = msgget(key, IPC_CREAT | 0666)) == -1) {
        perror("Error msgid\n");
        return 1;
    }

    return msgid;
}

// Decrypt dengan metode rot13
void rot13(FILE *file, char *str) {
    // Loop setiap karakter pada string
    for (int i=0; str[i]!='\0'; i++) {
        // Jika char berada dalam range A-M atau a-m, tambahkan ASCII dari char dengan 13
        if ((str[i] >= 'a' && str[i] <= 'm') || (str[i] >= 'A' && str[i] <= 'M')) {
            fputc((str[i] + 13), file);
        // Jika char berada dalam range N-Z atau n-z, kurangi ASCII dari char dengan 13
        } else if ((str[i] >= 'n' && str[i] <= 'z') || (str[i] >= 'N' && str[i] <= 'Z')) {
            fputc((str[i] - 13), file);
        }
        // Jika char selain huruf, maka tidak ada perubahan dan langsung dimasukkan ke dalam file
        else {
            fputc(str[i], file);
        }
    }
}

// Decrypt dengan metode base64
char *base64(char *input, size_t input_lenght, size_t *output_lenght) {
    // Deklarasi basic input output
    BIO *bio, *b64;
    // Alokasi mememori untuk output
    char *output = (char*)malloc(input_lenght);
    // Inisialisasi memori output menjadi 0
    memset(output, 0, input_lenght); 
    
    // Create objek BIO
    bio = BIO_new_mem_buf(input, input_lenght); 
    b64 = BIO_new(BIO_f_base64()); 
    // Mengubah data pada bio menjadi data pada b64
    bio = BIO_push(b64, bio); 
    
    // Set flags untuk menghilangkan karakter baris baru yang biasanya ditambahkan oleh metode base64
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);

    // Baca input dari BIO dan simpan dalam variabel output
    *output_lenght = BIO_read(bio, output, input_lenght); 
    
    BIO_free_all(bio); 
    return output;
}

// Decrypt dengan metode hex
void hex(FILE *file, char *str) {
    while (*str) {
        int value = 0;
        // Baca setiap 2 karakter
        for (int i=0; i<2 && *str; i++) {
            // Menggeser bit dari nilai value sebanyak 4 bit ke kiri.
            value <<= 4;
            // Cek apakah string merupakan bagian dari hexadesimal (0-9 dan A-F)
            if (*str >= '0' && *str <= '9') { 
                value |= (*str - '0');
            } else if (*str >= 'a' && *str <= 'f') {
                value |= (*str - 'a' + 10);
            } else if (*str >= 'A' && *str <= 'F') {
                value |= (*str - 'A' + 10);
            } else {
                fprintf(stderr, "Error: Invalid hexadecimal digit '%c'\n", *str);
                return;
            }
            str++;
        }
        // Write value ke dalam file
        fputc(value, file);
    }
}

// Fungsi dekripsi
void decrypt() {
    // Open file song-playlist.json dengan mode read ("r")
    FILE *songPlaylist = fopen("song-playlist.json", "r");
    // Open file playlist.txt dengan mode write ("w")
    FILE *playlist = fopen("playlist.txt", "w");

    // Cek jika file gagal dibuka maka print error
    if(songPlaylist == NULL) {
        printf("Error: Failed to open file song-playlist.json\n");
        return;
    }
    if(playlist == NULL) {
        printf("Error: Failed to open file playlist.txt\n");
        return;
    }

    // Parsing JSON
    struct json_object *jsonObj = json_object_from_file("song-playlist.json");

    // Jika parsing gagal print error
    if(jsonObj == NULL) {
        printf("Error: Failed to parse JSON\n");
        fclose(songPlaylist);
        fclose(playlist);
        return;
    }

    struct json_object *jsonMethod, *jsonSong;
    char *method, *song;
    int lenght = json_object_array_length(jsonObj);

    // Loop sebanyak lenght
    for(int i=0; i<lenght; i++) {
        // Mendapatkan objek JSON
        struct json_object *jsonIndex = json_object_array_get_idx(jsonObj, i);

        // Mendapatkan value "method" dan "song" dari objek JSON
        if(json_object_object_get_ex(jsonIndex, "method", &jsonMethod)) {
            method = (char *)json_object_get_string(jsonMethod);
        }
        if(json_object_object_get_ex(jsonIndex, "song", &jsonSong)) {
            song = (char *)json_object_get_string(jsonSong);
        }

        // Jalankan dekripsi sesuai metode yang diberikan user
        if(strcasecmp(method, "rot13") == 0) {
            rot13(playlist, song);
        }
        if(strcasecmp(method, "hex") == 0) {
            hex(playlist, song);
        }
        if(strcasecmp(method, "base64") == 0) {
            size_t outputSize;
            char *outputData = base64(song, strlen(song), &outputSize);
            fwrite(outputData, 1, outputSize, playlist);
            free(outputData);
        }
        
        fwrite("\n", 1, 1, playlist);
    }

    fclose(songPlaylist);
    fclose(playlist);
}

// Fungsi swap
void swap(char **a, char **b) {
    char *temp = *a;
    *a = *b;
    *b = temp;
}

// Fungsi partition
int partition(char **lines, int left, int right) {
    char *pivot = lines[right];
    int i = left - 1;
    for (int j=left; j<right; j++) {
        if (strcasecmp(lines[j], pivot) < 0) {
            i++;
            swap(&lines[i], &lines[j]);
        }
    }
    swap(&lines[i + 1], &lines[right]);
    return i + 1;
}

// Fungsi quicksort
void quicksort(char **lines, int left, int right) {
    if (left < right) {
        int pivot_idx = partition(lines, left, right);
        quicksort(lines, left, pivot_idx - 1);
        quicksort(lines, pivot_idx + 1, right);
    }
}

// Fungsi sorting
void sort() {
    int MAX_LINE_LEN = 10000;
    FILE *fp = fopen("playlist.txt", "r");
    if (!fp) {
        printf("Error: cannot open file playlist.txt\n");
        return;
    }

    char **lines = malloc(sizeof(char*) * MAX_LINE_LEN);
    int num_lines = 0;
    char line[MAX_LINE_LEN];
    while (fgets(line, MAX_LINE_LEN, fp) != NULL) {
        lines[num_lines] = strdup(line);
        num_lines++;
    }
    fclose(fp);

    quicksort(lines, 0, num_lines - 1);

    fp = fopen("playlist.txt", "w");
    for (int i=0; i<num_lines; i++) {
        fprintf(fp, "%s", lines[i]);
        free(lines[i]);
    }
    fclose(fp);
    free(lines);

    printf("File successfully decrypted and sorted.\n");
}

// Fungsi list untuk menampilkan semua list lagu yang ada pada file playlist.txt
void list() {
    // Open file playlist dalam mode read ("r")
    FILE *filePointer = fopen("playlist.txt", "r");
    // Check if the file was opened successfully
    if (filePointer == NULL) {
        printf("File cannot be opened.\n");
        return;
    }

    char ch;
    // Baca karakter di dalam file sampai EOF
    while ((ch = fgetc(filePointer)) != EOF)
        printf("%c", ch);

    fclose(filePointer);
}

// Fungsi play song
void play(char* word, int id) {
    // Open file playlist dalam mode read ("r")
    FILE* file = fopen("playlist.txt", "r");
    if (!file) {
        printf("Failed to open playlist.txt\n");
        return;
    }

    char line[1000];
    // Array yang menampung lagu yang sesuai
    char list_song[1000][1000]; 
    // Variabel counter untuk menyimpan song yang sesuai
    int found_count = 0;

    char search_word[1000];
    int i;
    for (i=0; word[i]; i++) {
        search_word[i] = tolower(word[i]);
    }
    search_word[i] = '\0';

    // Baca file setiap line
    while (fgets(line, 1000, file)) {
        int line_length = strlen(line);

        char lower_line[1000];
        int j;
        for (j=0; j<line_length; j++) { 
            lower_line[j] = tolower(line[j]);
        }
        lower_line[j] = '\0';

        // Jika search term ada pada line di dalam file
        if (strstr(lower_line, search_word) != NULL) {
            // Print/tambahkan lagu ke dalam list
            sprintf(list_song[found_count], "%s", line); 
            // Counter akan bertambah
            found_count++; 
        }
    }

    // Jika hanya ada 1 lagu yang sesuai 
    if(found_count == 1) { 
        printf("USER <%d> PLAYING \"%s\"\n", id, list_song[0]);
    }
    // Jika tidak ada lagu yang sesuai
    else if (found_count == 0) { 
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", word);
    }
    // Jika ada lebih dari 1 lagu yang sesuai
    else {
        printf("THERE ARE %d SONG CONTAINING \"%s\"\n", found_count, word);
        for (i=0; i<found_count; i++) {
            printf("%d. %s", i+1, list_song[i]);
        }
    }

    fclose(file);
}

// Fungsi add song
void add(char* word, int id) {    
    // Deklarasi
    // Open file dalam mode baca ("r")
    FILE *fp = fopen("playlist.txt", "r");    
    char line[100];    

    // Baca sertiap line di dalam file playlist.txt
    while (fgets(line, 100, fp) != NULL) {   
        // cek apakah lagu yang ditambahkan sudah ada di dalam playlist
        if (strcasecmp(line, word) == 0) {    
            // jika iya, print: SONG ALREADY ON PLAYLIST
            printf("SONG ALREADY ON PLAYLIST\n");    

            fclose(fp);  
            return;    
        }
    }

    fclose(fp);   

    // Open file dalam mode append ("a") untuk menambahkan data ke akhir file
    fp = fopen("playlist.txt", "a");    
    // Add song
    fprintf(fp, "%s\n", word);    
    fclose(fp);    
    printf("USER %d ADD %s\n", id, word);    
}

int main() {
    // Buat message queue
	int msgid = messageID();
    if(msgid == -1) {
        printf("Error: Failed to create message queue\n");
        exit(EXIT_FAILURE);
    }

    // Inisialisasi semaphore
    sem_t sem;
    sem_init(&sem, 0, 2);

    // Buat variabel bernama user Data untuk menyimpan command dari user
    struct user userData;

    while(1) {
        // Menerima message dari message queue
        if(msgrcv(msgid, &userData, sizeof(userData.command), 0, 0) == -1){ 
            perror("Failed at receiving message\n");
            exit(1);
        }
        // Set the user number = 1, anggap user sebagai user pertama
        usrnum = 1;

        sem_wait(&sem);

        // Update user id jika user berubah
        if(idUser1 != 1 && idUser2 == 2 && idUser1 != userData.id){
            idUser2 = userData.id;
        }

        if(idUser1 == 1 && idUser2 != userData.id) {
            idUser1 = userData.id;
            if(idUser2 == 1) {
                idUser2++;
            }
        }

        // Cek jika stream overload
        if(idUser1 != userData.id  && idUser2 != userData.id && idUser1 != 1 && idUser2 > 2) {
            printf("STREAM SYSTEM OVERLOAD\n");
            usrnum = 0;
        }

        // Jalankan command dari user
        if(usrnum == 1) {
            // Decrypt command
            if(strcasecmp(userData.command, "DECRYPT\n") == 0) {
                decrypt();
                sort();
            }
            // List command
            else if(strcasecmp(userData.command, "LIST\n") == 0) {
                list();
            }
            // Play dan Add command
            else if(strstr(userData.command, "PLAY") != NULL || strstr(userData.command, "ADD") != NULL) {
                char *start = strchr(userData.command, '"');
                if (start != NULL) {
                    start++;
                    char *end = strchr(start, '"');
                    if (end != NULL) {
                        int len = end - start;
                        char output[len + 1];
                        strncpy(output, start, len);
                        output[len] = '\0';

                        // play command
                        if(strstr(userData.command, "PLAY") != NULL) {
                            play(output, userData.id);
                        }
                        // Add command
                        if(strstr(userData.command, "ADD") != NULL) {
                            add(output, userData.id);
                        }
                    }
                }
            }
            // Unknown command
            else {
                printf("UNKNOWN COMMAND\n");
            }

            // Exit command
            if(strcasecmp(userData.command, "exit\n") == 0){
                if(idUser1 == userData.id) {
                    idUser1 = 1;
                    usrnum = 0;
                }
            else if(idUser2 == userData.id) {
                idUser2 = 2;
                usrnum = 0;
            }
        }
        }

        sem_post(&sem);
    }

    sem_destroy(&sem);

	return 0;
}