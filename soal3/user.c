#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>

struct user {
	int id;
	char command[1024];
};

// Message queue
int messageID() {
    key_t key;
    if((key = ftok("user", 65)) == -1) {
        perror("Error ftok\n");
        return 1;
    }

    int msgid;
    if((msgid = msgget(key, IPC_CREAT | 0666)) == -1) {
        perror("Error msgid\n");
        return 1;
    }

    return msgid;
}

int main() {
    // Inisialisasi message queue
	int msgid = messageID();
    if(msgid < 0) {
        printf("message queue failed\n");
        exit(EXIT_FAILURE);
    }
    
    while(1) {
        struct user userData;
        // Dapatkan id user
        userData.id = getpid();

        // Input command
        printf("ENTER COMMAND : ");
    	fgets(userData.command, 1024, stdin);

        // Break ketika command yang diberikan user adalah "exit"
        if(strcasecmp(userData.command, "exit\n") == 0)
            break;

        // Kirim message yang berisi command user ke message queue
        if(msgsnd(msgid, &userData, sizeof(userData.command), 0) == -1) {
            perror("Error msgsnd");
            exit(EXIT_FAILURE);
        };
    }

	return 0;
}