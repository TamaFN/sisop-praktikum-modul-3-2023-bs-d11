#define _GNU_SOURCE
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/stat.h>

// Membuat sebuah buffer sebesar 1024
#define BUFFER_SIZE 1024


//Fungsi ini digunakan untuk membuka file log.txt dalam mode append. 
//Fungsi ini juga menuliskan waktu saat ini pada awal baris log.
void clear_log() {
    if (access("log.txt", F_OK) != 0) return;
    remove("log.txt");
}

//Fungsi ini digunakan untuk membuka file log.txt dalam mode append. 
//Fungsi ini juga menuliskan waktu saat ini pada awal baris log.
FILE* open_log() {
    FILE* log = fopen("log.txt", "a");
    time_t current_time = time(NULL);
    struct tm* local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    fprintf(log, "%02d-%02d-%04d %02d:%02d:%02d ",
        local_time->tm_mday, local_time->tm_mon + 1, local_time->tm_year + 1900,
        local_time->tm_hour, local_time->tm_min, local_time->tm_sec
    );

    return log;
}

// Fungsi ini digunakan untuk menutup file log.txt.
void close_log(FILE* log) {
    fclose(log);
}

//Fungsi ini digunakan untuk menulis entri log ketika sebuah file atau direktori diakses.
void log_accessed(char* path) {
    FILE* log = open_log();
    fprintf(log, "ACCESSED %s\n", path);
    close_log(log);
}

//Fungsi ini digunakan untuk menulis entri log ketika sebuah direktori baru dibuat. 
void log_made(char* path) {
    FILE* log = open_log();
    fprintf(log, "MADE %s\n", path);
    close_log(log);
}

//Fungsi ini digunakan untuk menulis entri log ketika sebuah file dipindahkan ke direktori lain.
void log_moved(char* ext, char* src, char* dst) {
    FILE* log = open_log();
    fprintf(log, "MOVED %s file : %s > %s\n", ext, src, dst);
    close_log(log);
}

//Fungsi ini digunakan untuk memindahkan sebuah file dari src ke dst. 
//Fungsi ini juga memanggil fungsi log_moved() untuk menuliskan entri log. 
void move(char* ext, char* src, char* dst) {
    if (rename(src, dst) != 0) {
        printf("Failed to move %s to %s\n", src, dst);
        exit(EXIT_FAILURE);
    }
    else log_moved(ext, src, dst);
}

// Fungsi ini digunakan untuk membuat sebuah direktori pada path jika direktori tersebut belum ada. 
// Fungsi ini juga memanggil fungsi log_made() untuk menuliskan entri log.
void make(char* path) {
    if (access(path, F_OK) == 0) return;
    if (mkdir(path, 0777) != 0) {
        printf("Failed to make directory %s\n", path);
        exit(EXIT_FAILURE);
    }
    else log_made(path);
}

// Fungsi ini digunakan untuk mengubah string str menjadi lowercase.
char* lower_case(char* str) {
    for (int i = 0; str[i]; i++) str[i] = tolower(str[i]);
    return str;
}

// Fungsi ini digunakan untuk mendapatkan ekstensi dari sebuah file filename. 
// Fungsi ini mengembalikan string kosong jika file tersebut tidak memiliki ekstensi.
char* get_ext(char* filename) {
    char* ext = strrchr(filename, '.');
    if (ext == NULL) return "";
    return ext + 1;
}

// Fungsi ini merupakan fungsi rekursif yang berfungsi untuk mengkategorikan file-file dalam direktori src berdasarkan ekstensi ext. 
// Fungsi ini memindahkan file-file tersebut ke direktori categorized/ext dan memanggil fungsi make() dan move() untuk melakukan hal tersebut. 
// Fungsi ini juga melakukan pengecekan dan rekursi pada direktori dalam src. 
// Fungsi ini juga memanggil fungsi log_accessed() untuk menuliskan entri log.
void categorize(char* src, char* ext, int* cnt, int max) {
    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    if (src_dir == NULL) {
        printf("Failed to open directory %s\n", src);
        exit(EXIT_FAILURE);
    }
    else log_accessed(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char new_src[BUFFER_SIZE];
            snprintf(new_src, BUFFER_SIZE, "%s/%s", src, src_ent->d_name);
            categorize(new_src, ext, cnt, max);
        }

        else if (src_ent->d_type == DT_REG) {
            char src_ext[10];
            strcpy(src_ext, get_ext(src_ent->d_name));
            if (strcmp(ext, "other") != 0 && strcmp(lower_case(src_ext), ext) != 0) continue;
            
            int dst_cnt = *cnt / 10 + 1;
            char dst[BUFFER_SIZE];
            
            if (dst_cnt > 1) snprintf(dst, BUFFER_SIZE, "categorized/%s (%d)", ext, dst_cnt);
            else snprintf(dst, BUFFER_SIZE, "categorized/%s", ext);
            make(dst);

            char filename[BUFFER_SIZE];
            snprintf(filename, BUFFER_SIZE, "%s/%s", src, src_ent->d_name);
            strcat(dst, "/");
            strcat(dst, src_ent->d_name);
            move(ext, filename, dst);
            (*cnt)++;
        }
    }

    closedir(src_dir);
}

// Membuat deklarasi struct 
typedef struct {
    char name[BUFFER_SIZE];
    int cnt;
} Extensions;

// Fungsi ini digunakan untuk melakukan sorting pada array exts berdasarkan jumlah file yang terkategorikan.
// Fungsi ini mengurutkan array exts dari yang terkecil ke terbesar.
void sort_exts(Extensions* exts, int size) {
    Extensions temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (exts[i].cnt < exts[j].cnt) continue;
            temp = exts[i];
            exts[i] = exts[j];
            exts[j] = temp;
        }
    }
}

typedef struct {
    char src[BUFFER_SIZE];
    char ext[10];
    int max;
} ThreadArgs;

// Fungsi ini merupakan fungsi yang dijalankan oleh setiap thread yang dibuat pada program ini. 
// Fungsi ini memanggil fungsi categorize() untuk mengkategorikan file-file dalam direktori. 
// Fungsi ini juga mengembalikan struct Extensions yang berisi jumlah file yang terkategorikan berdasarkan ekstensi.
void* thread_function(void* arg) {
    ThreadArgs* args = (ThreadArgs*) arg;
    int cnt = 0;
    categorize(args->src, args->ext, &cnt, args->max);

    Extensions* result = (Extensions*) malloc(sizeof(Extensions));
    strcpy(result->name, args->ext);
    result->cnt = cnt;

    return result;
}

// Fungsi utama dari program ini membaca file extensions.txt dan max.txt untuk mendapatkan daftar ekstensi dan batasan jumlah file yang terkategorikan untuk setiap ekstensi. 
// Fungsi ini juga membuat sebuah thread untuk setiap ekstensi yang ada dan melakukan sorting pada array exts untuk menampilkan daftar ekstensi yang terkategorikan beserta jumlah file yang terkategorikan.
int main() {
    clear_log();
    make("categorized");

    FILE* ext_fp = fopen("extensions.txt", "r");
    FILE* max_fp = fopen("max.txt", "r");

    int max;
    fscanf(max_fp, "%d", &max);

    char ext[10];
    int exts_size = 0;
    pthread_t tid[BUFFER_SIZE];
    Extensions exts[BUFFER_SIZE];
    ThreadArgs args[BUFFER_SIZE];

    while (fscanf(ext_fp, "%s", ext) != EOF) {
        strcpy(args[exts_size].src, "files");
        strcpy(args[exts_size].ext, ext);
        args[exts_size].max = max;
        pthread_create(&tid[exts_size], NULL, thread_function, &args[exts_size]);
        exts_size++;
    }

    for (int i = 0; i < exts_size; i++) {
        Extensions* result;
        pthread_join(tid[i], (void **) &result);
        exts[i] = *result;
    }

    int cnt = 0;
    categorize("files", "other", &cnt, max);
    strcpy(exts[exts_size].name, "other");
    exts[exts_size].cnt = cnt;
    exts_size++;

    sort_exts(exts, exts_size);
    for (int i = 0; i < exts_size; i++) {
        printf("%s : %d\n", exts[i].name, exts[i].cnt);
    }

    return 0;
}

