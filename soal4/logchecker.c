#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFER_SIZE 1024

typedef struct {
    char name[BUFFER_SIZE];
    int cnt;
} Pair;

void sort_pairs(Pair* folders, int size) {
    Pair temp;
    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            if (folders[i].cnt < folders[j].cnt) continue;
            temp = folders[i];
            folders[i] = folders[j];
            folders[j] = temp;
        }
    }
}

int main() {
    system("echo Total ACCESSED: $(cat log.txt | grep ACCESSED | wc -l)");

    FILE* folders_fp = popen("awk -F 'MADE ' '/MADE/ {print $2}' log.txt", "r");
    FILE* exts_fp = fopen("extensions.txt", "r");
    char buf[BUFFER_SIZE];

    printf("\nFolder Created : Total Files\n\n");

    int folders_size = 0;
    Pair folders[BUFFER_SIZE];
    while(fscanf(folders_fp, "%[^\n]%*c", buf) != EOF) {
        if (strcmp(buf, "") == 0) break;
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep '%s' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(folders[folders_size].name, buf);
        folders[folders_size].cnt = cnt;
        folders_size++;
    }

    sort_pairs(folders, folders_size);
    for (int i = 0; i < folders_size; i++) {
        printf("%s : %d\n", folders[i].name, folders[i].cnt);
    }

    printf("\nExtension Types : Total Files\n\n");

    int exts_size = 0;
    Pair exts[BUFFER_SIZE];
    while (fscanf(exts_fp, "%s", buf) != EOF) {
        char command[2*BUFFER_SIZE+100];
        sprintf(command, "cat log.txt | grep MOVED | grep -i '\\.%s$' | wc -l", buf);

        int cnt;
        FILE* cnt_fp = popen(command, "r");
        fscanf(cnt_fp, "%d", &cnt);

        strcpy(exts[exts_size].name, buf);
        exts[exts_size].cnt = cnt;
        exts_size++;
    }

    sort_pairs(exts, exts_size);
    for (int i = 0; i < exts_size; i++) {
        printf("%s : %d\n", exts[i].name, exts[i].cnt);
    }

    pclose(folders_fp);

    return 0;
}

